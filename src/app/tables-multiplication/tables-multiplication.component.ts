import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-tables-multiplication',
  templateUrl: './tables-multiplication.component.html',
  styleUrls: ['./tables-multiplication.component.scss']
})
export class TablesMultiplicationComponent implements OnInit {

  // Le composant reçoit en entrée le nombre de tables depuis le composant parent
  @Input() nombreDeTables!: number;

  // retourne un array de nombre, si nombreDeTables = 4, retourne [0, 1, 2, 3]
  counter(i: number) {
    return new Array(i);
  }
  constructor() { }

  ngOnInit(): void {
  }

}
