import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-multiplication',
  templateUrl: './table-multiplication.component.html',
  styleUrls: ['./table-multiplication.component.scss']
})
export class TableMultiplicationComponent implements OnInit {

  @Input() nombre!: number;

  constructor() { }

  ngOnInit(): void {
  }

}
